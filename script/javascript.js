fetch('https://api.mockaroo.com/api/f0e144d0?count=10&key=ae412dc0')
    .then(response => response.json())
    .then(data => {
        const tableBody = document.querySelector(".body");
        for (let i = 0; i < data.length; i++) {
            const personData = data[i]

            const newRow = document.createElement("tr");
            if (i % 2 == 0) {
                newRow.className = "body__row body__row--odd";
            } else {
                newRow.className = "body__row body__row--even"
            }

            const newImgCell = document.createElement("td");
            newImgCell.className = "body__cell body__cell-img";
            const newImg = document.createElement("img");
            newImg.setAttribute("src", personData.avatar);
            newImg.setAttribute("alt", "avatar");
            newImg.className = "image";
            newImgCell.appendChild(newImg);
            newRow.appendChild(newImgCell);

            const newNameCell = document.createElement("td");
            newNameCell.className = "body__cell";
            const newNameTextField = document.createTextNode(personData.name);
            newNameCell.appendChild(newNameTextField);
            newRow.appendChild(newNameCell);

            const newLocationCell = document.createElement("td");
            newLocationCell.className = "body__cell";
            const newLocationTextField = document.createTextNode(personData.location);
            newLocationCell.appendChild(newLocationTextField);
            newRow.appendChild(newLocationCell)

            const newEmailCell = document.createElement("td");
            newEmailCell.className = "body__cell";
            const newEmailTextField = document.createTextNode(personData.email);
            newEmailCell.appendChild(newEmailTextField);
            newRow.appendChild(newEmailCell);

            const newPhoneCell = document.createElement("td");
            newPhoneCell.className = "body__cell";
            const newPhoneTextField = document.createTextNode(personData.number);
            newPhoneCell.appendChild(newPhoneTextField);
            newRow.appendChild(newPhoneCell);

            console.log(newRow)
            tableBody.appendChild(newRow);
            console.log(tableBody)

        }
    })
